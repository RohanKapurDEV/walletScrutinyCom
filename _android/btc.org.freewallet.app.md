---
title: "Bitcoin Wallet. Buy & Exchange BTC coin－Freewallet"
altTitle: 

users: 500000
appId: btc.org.freewallet.app
launchDate: 2016-06-13
latestUpdate: 2020-04-21
apkVersionName: "2.5.2"
stars: 3.9
ratings: 3675
reviews: 1642
size: 7.2M
website: https://freewallet.org/
repository: 
issue: 
icon: btc.org.freewallet.app.png
bugbounty: 
verdict: custodial # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2019-12-20
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /btc.org.freewallet.app/
  - /posts/btc.org.freewallet.app/
---


According to their description on Google Play, this is a custodial app:

> The Freewallet team keeps most of our customers’ coins in offline cold storage
to ensure the safety of your funds.

Our verdict: This app is **not verifiable**.
