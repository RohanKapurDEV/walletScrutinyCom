---
title: "Coins.ph Wallet"
altTitle: 

users: 5000000
appId: asia.coins.mobile
launchDate: 2014-10-01
latestUpdate: 2020-10-02
apkVersionName: "3.4.5"
stars: 3.8
ratings: 77741
reviews: 34927
size: 48M
website: https://coins.ph/
repository: 
issue: 
icon: asia.coins.mobile.png
bugbounty: 
verdict: custodial # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-03-28
reviewStale: true
signer: 
reviewArchive:
- date: 2019-11-17
  version: "3.3.92"
  apkHash: 
  gitRevision: 372c9c03c6422faed457f1a9975d7cab8f13d01f
  verdict: nosource

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /coinsph/
  - /asia.coins.mobile/
  - /posts/2019/11/coinsph/
  - /posts/asia.coins.mobile/
---


Coins.ph Wallet
being a very broad product and not strongly focused on being a Bitcoin wallet
does not emphasize being an actual wallet by our definition and even if it was,
the lack of source code makes it impossible to verify this app.

Our verdict: This "wallet" is probably custodial but does not provide public source
and therefore is **not verifiable**.
