---
title: "Zumo - Bitcoin Wallet App - Buy, Store, & Transfer"
altTitle: 

users: 500
appId: com.zumopay.core
launchDate: 
latestUpdate: 2020-09-15
apkVersionName: "2.8.0"
stars: 4.5
ratings: 21
reviews: 13
size: 59M
website: https://zumo.money/
repository: 
issue: 
icon: com.zumopay.core.png
bugbounty: 
verdict: fewusers # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-05-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: zumopay
providerLinkedIn: company/zumomoney
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.zumopay.core/
  - /posts/com.zumopay.core/
---


