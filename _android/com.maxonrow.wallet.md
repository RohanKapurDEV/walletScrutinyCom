---
title: "MAX-Wallet"
altTitle: 

users: 5000
appId: com.maxonrow.wallet
launchDate: 2019-08-31
latestUpdate: 2020-06-02
apkVersionName: "2.0.05"
stars: 4.1
ratings: 65
reviews: 37
size: 41M
website: https://www.maxonrow.com
repository: 
issue: 
icon: com.maxonrow.wallet.png
bugbounty: 
verdict: nobtc # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2019-12-30
reviewStale: true
signer: 
reviewArchive:


providerTwitter: maxonrow
providerLinkedIn: company/maxonrow
providerFacebook: maxonrowblockchain
providerReddit: 

redirect_from:
  - /com.maxonrow.wallet/
  - /posts/com.maxonrow.wallet/
---


We find no mention of Bitcoin on the description or the website.