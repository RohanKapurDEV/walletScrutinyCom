---
title: "Bit2Me - Buy and Sell Cryptocurrencies"
altTitle: 

users: 10000
appId: com.phonegap.bit2me
launchDate: 
latestUpdate: 2020-06-10
apkVersionName: "1.36.1"
stars: 2.9
ratings: 161
reviews: 110
size: 5.0M
website: https://bit2me.com/
repository: 
issue: 
icon: com.phonegap.bit2me.png
bugbounty: 
verdict: custodial # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-05-29
reviewStale: true
signer: 
reviewArchive:


providerTwitter: bit2me
providerLinkedIn: company/bit2me
providerFacebook: bit2me
providerReddit: 

redirect_from:
  - /com.phonegap.bit2me/
  - /posts/com.phonegap.bit2me/
---


This appears to be the interface for an exchange. We could not find any claims
about you owning your keys. As a custodial service it is **not verifiable**.
