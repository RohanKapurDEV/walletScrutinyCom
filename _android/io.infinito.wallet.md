---
title: "Infinito Wallet - Crypto Wallet & DApp Browser"
altTitle: 

users: 100000
appId: io.infinito.wallet
launchDate: 2017-11-15
latestUpdate: 2020-09-24
apkVersionName: "2.31.0"
stars: 4.2
ratings: 2025
reviews: 968
size: 59M
website: https://www.infinitowallet.io
repository: 
issue: 
icon: io.infinito.wallet.png
bugbounty: 
verdict: nosource # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-03-30
reviewStale: true
signer: 
reviewArchive:


providerTwitter: Infinito_Ltd
providerLinkedIn: company/infinitoservices/
providerFacebook: InfinitoWallet
providerReddit: 

redirect_from:
  - /io.infinito.wallet/
  - /posts/io.infinito.wallet/
---


Right on the Playstore description we find:

> Truly decentralized: You control private key, passphrases and passwords

So it is not a custodial app. How about source code?

Searching for its appId on GitHub
[does not yield promising results](https://github.com/search?q=%22io.infinito.wallet%22&type=Code)
neither.

Our verdict: This app is **not verifiable**.
