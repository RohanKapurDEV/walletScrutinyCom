---
title: "Bitcoin Wallet & Ethereum Ripple Tron EOS"
altTitle: 

users: 100000
appId: io.atomicwallet
launchDate: 2019-01-30
latestUpdate: 2020-09-21
apkVersionName: "0.64.0"
stars: 4.5
ratings: 14701
reviews: 7889
size: 13M
website: https://atomicwallet.io/
repository: 
issue: 
icon: io.atomicwallet.png
bugbounty: 
verdict: nosource # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2019-11-16
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /atomicwallet/
  - /io.atomicwallet/
  - /posts/2019/11/atomicwallet/
  - /posts/io.atomicwallet/
---


Bitcoin Wallet & Ethereum Ripple Tron EOS
is a non-custodial wallet according to their description:

> Atomic Wallet is universal non-custodial app for over 300 cryptocurrencies.

Unfortunately they do not share all sources for the Android app.

Verdict: This wallet is **not verifiable**.
