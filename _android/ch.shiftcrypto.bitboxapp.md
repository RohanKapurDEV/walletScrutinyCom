---
title: "BitBoxApp"
altTitle: 

users: 100
appId: ch.shiftcrypto.bitboxapp
launchDate: 
latestUpdate: 2020-09-22
apkVersionName: "android-4.22.0"
stars: 4.3
ratings: 6
reviews: 3
size: 64M
website: 
repository: 
issue: 
icon: ch.shiftcrypto.bitboxapp.png
bugbounty: 
verdict: fewusers # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-08-28
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /ch.shiftcrypto.bitboxapp/
---


