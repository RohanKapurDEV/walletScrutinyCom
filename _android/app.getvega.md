---
title: "Vega - Lightning Wallet"
altTitle: 

users: 10
appId: app.getvega
launchDate: 
latestUpdate: 2019-05-22
apkVersionName: "Varies with device"
stars: 
ratings: 
reviews: 
size: Varies with device
website: https://getvega.app
repository: 
issue: 
icon: app.getvega.png
bugbounty: 
verdict: fewusers # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2019-12-29
reviewStale: false
signer: 
reviewArchive:


providerTwitter: GetVegaApp
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /app.getvega/
  - /posts/app.getvega/
---


