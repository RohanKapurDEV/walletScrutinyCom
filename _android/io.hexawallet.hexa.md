---
title: "Bitcoin Wallet Hexa"
altTitle: 

users: 100
appId: io.hexawallet.hexa
launchDate: 
latestUpdate: 2020-09-23
apkVersionName: "1.1.2"
stars: 4.2
ratings: 6
reviews: 2
size: 35M
website: 
repository: 
issue: 
icon: io.hexawallet.hexa.png
bugbounty: 
verdict: fewusers # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-08-10
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /io.hexawallet.hexa/
---


This page was created by a script from the **appId** "io.hexawallet.hexa" and public
information found
[here](https://play.google.com/store/apps/details?id=io.hexawallet.hexa).

Probably an engineer will soon have a deeper look at this app.

So far we are not even sure it is a wallet ... Please check back later.